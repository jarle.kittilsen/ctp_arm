.section .text
.global _start

_start:
    mov r0, #0x4
    add r1, r0, r0
    subs r2, r1, #0x8
    addeq r2, r2, #0x10
    bkpt

    @TODO 1: Attach gdb and single step, observe the registers and flags

    @TODO 2: Add instructions to subtract 2 from r2, store the result in r3. Double the value in r3 by using a mov and the barrel-shifter. 
    @        You should now have 28 (0x1c) in r3
    
    @TODO 3: (Bonus) Play around. E.g. Try conditionals
 	
