.data   
string: .asciz "lvmj}{ncja{"  /* We put a null-terminated string in memory */

.text    
.global _start

_start:
    ldr r0, adr_string  @ load the memory address of the string via label adr_string into R0
    mov r3, r0		@ Take a copy of the address, just for convenience when debugging
    bl my_func
end:
    eor r0, r0 		@ exit with r0=0 (Success). Also good place to set breakpoint in gdb
    bkpt             

my_func:
    @TODO: Create the function to XOR the string with 0xf

/* We put the address of string into the literal pool, with label adr_atring*/
adr_string: .word string
